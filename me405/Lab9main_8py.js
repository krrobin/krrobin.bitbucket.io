var Lab9main_8py =
[
    [ "curr_time", "Lab9main_8py.html#a350b3fbafafc793c4fdc5b2ce8ec031b", null ],
    [ "duty_conv", "Lab9main_8py.html#a506dd494ccfe2e2cab64eee08acc6d31", null ],
    [ "Duty_x", "Lab9main_8py.html#a1e9c2013065c78572663f815661a2c6d", null ],
    [ "Duty_y", "Lab9main_8py.html#a6f73ff71ca2b2bff908ae373487d6346", null ],
    [ "enc1", "Lab9main_8py.html#aaf8c8aab769ddfb7bee4c8935f68e4ba", null ],
    [ "enc2", "Lab9main_8py.html#a547629761cb6c5285bb81b24e52ab026", null ],
    [ "K1", "Lab9main_8py.html#a2a705f22142a172b9c036762962eddca", null ],
    [ "K2", "Lab9main_8py.html#a3ab4d296133adafca521befcae3f421e", null ],
    [ "K3", "Lab9main_8py.html#aa312499b174797d1fc380c6c0583ebcd", null ],
    [ "K4", "Lab9main_8py.html#a55763e698d8b935e31c209dcb0076ab6", null ],
    [ "Kt", "Lab9main_8py.html#a6aba999edf64486cd36457f29633e7dd", null ],
    [ "moe1", "Lab9main_8py.html#af17f16c784adfbe8edc876f02f6f9d71", null ],
    [ "moe2", "Lab9main_8py.html#aceb84bfd8abe2b077b2855a5a60db592", null ],
    [ "R", "Lab9main_8py.html#aa27aa9edcf299c45257e617cec922744", null ],
    [ "start_time", "Lab9main_8py.html#aee2ccc1798a68ae113fc7e9ec5d877c0", null ],
    [ "theta_dot_x", "Lab9main_8py.html#ab825b7a254293692038cf90807156a7d", null ],
    [ "theta_dot_y", "Lab9main_8py.html#a85901a2212ff25fe2f738ae051109e3d", null ],
    [ "theta_x", "Lab9main_8py.html#a1fc38d09692cb02f3225f4f862d72f19", null ],
    [ "theta_x_new", "Lab9main_8py.html#a9767fc31e3c9dbe269e884b1054a5742", null ],
    [ "theta_y", "Lab9main_8py.html#a8c6559a366d705994712bdc000225c01", null ],
    [ "theta_y_new", "Lab9main_8py.html#a6a8d9c3d57db7962730b5461039d22fc", null ],
    [ "timespan", "Lab9main_8py.html#adbb7d7dfae9a3d715ef7c92d758a8a89", null ],
    [ "Torque_x", "Lab9main_8py.html#a41ffb4f03517501577c2effc83945fff", null ],
    [ "Torque_y", "Lab9main_8py.html#a2dc0f43612e31ba78a63d0bb37363a4c", null ],
    [ "touch", "Lab9main_8py.html#ac89f340b9fcb3297d443382cf5f4329c", null ],
    [ "Vdc", "Lab9main_8py.html#a55fd3283e34fde82ca15261117adc75e", null ],
    [ "x", "Lab9main_8py.html#a471fd28d1d37a6aba223db0dffa3df2c", null ],
    [ "x_dot", "Lab9main_8py.html#af413511e5a722d146c73f1c4cd055626", null ],
    [ "x_new", "Lab9main_8py.html#a59aac6cab7934693c625bd32212f6609", null ],
    [ "y", "Lab9main_8py.html#aac221adc9700dc07802ebe91a7eac166", null ],
    [ "y_dot", "Lab9main_8py.html#a1566a44ec905ac94dbba507f68acb88f", null ],
    [ "y_new", "Lab9main_8py.html#a88082a4fe84c1fa0cb83a7a417e0c78e", null ]
];