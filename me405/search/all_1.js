var searchData=
[
  ['adc_1',['adc',['../lab3main_8py.html#a8b59356ee3e0e964f8227401b6fc1c38',1,'lab3main']]],
  ['adc1_2',['adc1',['../lab4main_8py.html#a87dc6ae28b17b07ba3a6140955ccc1ed',1,'lab4main']]],
  ['adc_5fxm_3',['ADC_xm',['../screendriver_8py.html#aec1376726c257dc325b925fee32fc6e5',1,'screendriver']]],
  ['adc_5fym_4',['ADC_ym',['../screendriver_8py.html#a9a75d4b10d36d6e5c9119066cc934c3c',1,'screendriver']]],
  ['address_5',['address',['../lab4main_8py.html#a835a21606477234f092051643abf7cf9',1,'lab4main.address()'],['../mcp9808_8py.html#a089290bc0c9e4923b9c6d0b9368eddd3',1,'mcp9808.address()']]],
  ['all_5fscans_6',['All_Scans',['../classTouchDriver_1_1TouchClass.html#adad320310fa3e1341c517bb61dc75166',1,'TouchDriver::TouchClass']]],
  ['ambient_7',['ambient',['../lab4main_8py.html#a762136809b99d784ac9fbc9e0e0a39d3',1,'lab4main']]],
  ['asciival_8',['asciival',['../mcp9808_8py.html#a886781ce4d0ce303743334d89db87b27',1,'mcp9808']]],
  ['avg_5ftime_9',['avg_time',['../ReactionTime_8py.html#a540850bcbd37bdee5f0952eabcad0c6c',1,'ReactionTime']]]
];
