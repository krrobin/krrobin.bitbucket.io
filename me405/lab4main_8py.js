var lab4main_8py =
[
    [ "adc1", "lab4main_8py.html#a87dc6ae28b17b07ba3a6140955ccc1ed", null ],
    [ "address", "lab4main_8py.html#a835a21606477234f092051643abf7cf9", null ],
    [ "ambient", "lab4main_8py.html#a762136809b99d784ac9fbc9e0e0a39d3", null ],
    [ "Ambient_degC", "lab4main_8py.html#ab5e621e61827991ba953abc53cd88f3b", null ],
    [ "curr_time", "lab4main_8py.html#a55fe4661c02cf3315288cb661255cce3", null ],
    [ "i2c", "lab4main_8py.html#a442d96d63a3f3e606e1553312bed4f87", null ],
    [ "mcp", "lab4main_8py.html#af645c155fd48c2cf08dc3ce28f1589bf", null ],
    [ "mcu_vref", "lab4main_8py.html#a1a4bd677a30028603115aacf511f123a", null ],
    [ "next_time", "lab4main_8py.html#acbfaba2284d58214699076ac69d3c0b0", null ],
    [ "start_time", "lab4main_8py.html#a6cae066aa4118751bd80ca451db883ce", null ],
    [ "stm_list", "lab4main_8py.html#aca412a8b590e8f6de60b73632b5d529c", null ],
    [ "STM_Temp_degC", "lab4main_8py.html#acff3297255fcc481065f01b155c357e9", null ]
];