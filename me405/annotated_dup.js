var annotated_dup =
[
    [ "encoder_driver", null, [
      [ "encoder", "classencoder__driver_1_1encoder.html", "classencoder__driver_1_1encoder" ]
    ] ],
    [ "mcp9808", null, [
      [ "MCP9808", "classmcp9808_1_1MCP9808.html", "classmcp9808_1_1MCP9808" ]
    ] ],
    [ "motor_driver", null, [
      [ "MotorDriver", "classmotor__driver_1_1MotorDriver.html", "classmotor__driver_1_1MotorDriver" ]
    ] ],
    [ "screendriver", null, [
      [ "touchscreen", "classscreendriver_1_1touchscreen.html", "classscreendriver_1_1touchscreen" ]
    ] ],
    [ "TouchDriver", null, [
      [ "TouchClass", "classTouchDriver_1_1TouchClass.html", "classTouchDriver_1_1TouchClass" ]
    ] ]
];