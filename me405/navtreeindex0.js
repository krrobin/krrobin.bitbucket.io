var NAVTREEINDEX0 =
{
"Lab5HandCalculations.html":[10],
"Lab5HandCalculations.html#sec_lab5details":[10,0],
"Lab6Plots.html":[11],
"Lab6Plots.html#sec_lab6details":[11,0],
"Lab9Analytics.html":[12],
"Lab9Analytics.html#sec_lab9analytics":[12,0],
"Lab9main_8py.html":[14,0,3],
"Lab9main_8py.html#a1566a44ec905ac94dbba507f68acb88f":[14,0,3,30],
"Lab9main_8py.html#a1e9c2013065c78572663f815661a2c6d":[14,0,3,2],
"Lab9main_8py.html#a1fc38d09692cb02f3225f4f862d72f19":[14,0,3,17],
"Lab9main_8py.html#a2a705f22142a172b9c036762962eddca":[14,0,3,6],
"Lab9main_8py.html#a2dc0f43612e31ba78a63d0bb37363a4c":[14,0,3,23],
"Lab9main_8py.html#a350b3fbafafc793c4fdc5b2ce8ec031b":[14,0,3,0],
"Lab9main_8py.html#a3ab4d296133adafca521befcae3f421e":[14,0,3,7],
"Lab9main_8py.html#a41ffb4f03517501577c2effc83945fff":[14,0,3,22],
"Lab9main_8py.html#a471fd28d1d37a6aba223db0dffa3df2c":[14,0,3,26],
"Lab9main_8py.html#a506dd494ccfe2e2cab64eee08acc6d31":[14,0,3,1],
"Lab9main_8py.html#a547629761cb6c5285bb81b24e52ab026":[14,0,3,5],
"Lab9main_8py.html#a55763e698d8b935e31c209dcb0076ab6":[14,0,3,9],
"Lab9main_8py.html#a55fd3283e34fde82ca15261117adc75e":[14,0,3,25],
"Lab9main_8py.html#a59aac6cab7934693c625bd32212f6609":[14,0,3,28],
"Lab9main_8py.html#a6a8d9c3d57db7962730b5461039d22fc":[14,0,3,20],
"Lab9main_8py.html#a6aba999edf64486cd36457f29633e7dd":[14,0,3,10],
"Lab9main_8py.html#a6f73ff71ca2b2bff908ae373487d6346":[14,0,3,3],
"Lab9main_8py.html#a85901a2212ff25fe2f738ae051109e3d":[14,0,3,16],
"Lab9main_8py.html#a88082a4fe84c1fa0cb83a7a417e0c78e":[14,0,3,31],
"Lab9main_8py.html#a8c6559a366d705994712bdc000225c01":[14,0,3,19],
"Lab9main_8py.html#a9767fc31e3c9dbe269e884b1054a5742":[14,0,3,18],
"Lab9main_8py.html#aa27aa9edcf299c45257e617cec922744":[14,0,3,13],
"Lab9main_8py.html#aa312499b174797d1fc380c6c0583ebcd":[14,0,3,8],
"Lab9main_8py.html#aac221adc9700dc07802ebe91a7eac166":[14,0,3,29],
"Lab9main_8py.html#aaf8c8aab769ddfb7bee4c8935f68e4ba":[14,0,3,4],
"Lab9main_8py.html#ab825b7a254293692038cf90807156a7d":[14,0,3,15],
"Lab9main_8py.html#ac89f340b9fcb3297d443382cf5f4329c":[14,0,3,24],
"Lab9main_8py.html#aceb84bfd8abe2b077b2855a5a60db592":[14,0,3,12],
"Lab9main_8py.html#adbb7d7dfae9a3d715ef7c92d758a8a89":[14,0,3,21],
"Lab9main_8py.html#aee2ccc1798a68ae113fc7e9ec5d877c0":[14,0,3,14],
"Lab9main_8py.html#af17f16c784adfbe8edc876f02f6f9d71":[14,0,3,11],
"Lab9main_8py.html#af413511e5a722d146c73f1c4cd055626":[14,0,3,27],
"ReactionTime_8py.html":[14,0,6],
"ReactionTime_8py.html#a1ac1c26265f6efcaee129b59ae227079":[14,0,6,7],
"ReactionTime_8py.html#a540850bcbd37bdee5f0952eabcad0c6c":[14,0,6,1],
"ReactionTime_8py.html#a83fb23e41e4a274c0dd396b416be22b7":[14,0,6,4],
"ReactionTime_8py.html#a8d93518afc5b2c7fc5dc6ff34b58456d":[14,0,6,5],
"ReactionTime_8py.html#ab99a3cc5c8e3cfc357108848c6e0636c":[14,0,6,0],
"ReactionTime_8py.html#abde044a6ff1854ae4a39961b6a2b889a":[14,0,6,3],
"ReactionTime_8py.html#ad04bfe67be5692c6251830c066eba0ee":[14,0,6,6],
"ReactionTime_8py.html#ad0ba5ad15b366aa17a137c99594d64f2":[14,0,6,8],
"ReactionTime_8py.html#adff87607cd4bd1367f1e7f991f01d642":[14,0,6,2],
"TouchDriver_8py.html":[14,0,8],
"TouchDriver_8py.html#a0da99a8f8acb020c8ec77c3c38f531ff":[14,0,8,3],
"TouchDriver_8py.html#a8f80563bb0a0fa04c43cb16157d7598e":[14,0,8,4],
"TouchDriver_8py.html#a9d5ac4c27b4ca039fe837488b3be98e7":[14,0,8,1],
"TouchDriver_8py.html#ac2d7ab5b5d6da321073ffdde714dbf22":[14,0,8,2],
"UI__Front_8py.html":[14,0,9],
"UI__Front_8py.html#a3b9c9016acf4bddd68ddb58b90ffd61d":[14,0,9,1],
"UI__Front_8py.html#a4869d59882a7ded941db7f80441eea04":[14,0,9,2],
"UI__Front_8py.html#a85fe64b1dfaf65bfbfa3d126b7c6ab54":[14,0,9,5],
"UI__Front_8py.html#a86cf4592546e1f8a35c12a7147b256a1":[14,0,9,4],
"UI__Front_8py.html#aa86465327c55b9cb4e854e117320f933":[14,0,9,0],
"UI__Front_8py.html#afa66f96f75524623b17c188acf7ba539":[14,0,9,3],
"VendingMachine_8py.html":[14,0,10],
"VendingMachine_8py.html#a0bdd583ed6b646d6b40ec6764987da7f":[14,0,10,0],
"VendingMachine_8py.html#a1394a04c8e8e852cd35fa9da0f7ddac9":[14,0,10,1],
"VendingMachine_8py.html#a1fd5c5bf0f87fd84ea58fd26e3dd4e62":[14,0,10,15],
"VendingMachine_8py.html#a24d00013189f65d7975c3060bcae6cd9":[14,0,10,13],
"VendingMachine_8py.html#a2d4e347c0915b02c65b294c02242feda":[14,0,10,4],
"VendingMachine_8py.html#a39feea0cfccdfffcb9a3cdcc7222cc67":[14,0,10,9],
"VendingMachine_8py.html#a4a13532821d3684b59d56d9d2c28f37d":[14,0,10,10],
"VendingMachine_8py.html#a5ef33750c80f0dda32337f694251d0c2":[14,0,10,3],
"VendingMachine_8py.html#a783dd23e008fd36d710a02f49f4e7a91":[14,0,10,11],
"VendingMachine_8py.html#a906ec0ca3831e34828b9941c4542d1c3":[14,0,10,6],
"VendingMachine_8py.html#abe58c3400c2a13dc5d5425b2d2c64e0f":[14,0,10,7],
"VendingMachine_8py.html#abfc71baf7cfab5f018c168d44a2d92fb":[14,0,10,2],
"VendingMachine_8py.html#ac2c920ff99b51090ae785f6e2fffa87a":[14,0,10,14],
"VendingMachine_8py.html#ad2801c081c0bd92f81e4d3c27c684c34":[14,0,10,5],
"VendingMachine_8py.html#ae5d257da0cad71f362e98585f63579dd":[14,0,10,8],
"VendingMachine_8py.html#aec41fdce1a0cef6616a8b2f9a8f647d1":[14,0,10,16],
"VendingMachine_8py.html#aeed0d4b5102a194c6a60cbdfd2d29fde":[14,0,10,12],
"annotated.html":[13,0],
"classTouchDriver_1_1TouchClass.html":[13,0,4,0],
"classTouchDriver_1_1TouchClass.html#a1234df2af2cb1ea2301c7c3c64b412be":[13,0,4,0,0],
"classTouchDriver_1_1TouchClass.html#a328ec11791cbc097528683ccf2fdc1b6":[13,0,4,0,2],
"classTouchDriver_1_1TouchClass.html#a35582fea67eb8406febf628194eba390":[13,0,4,0,13],
"classTouchDriver_1_1TouchClass.html#a3b9b925c0fb1fb9c69a688aa139228bd":[13,0,4,0,3],
"classTouchDriver_1_1TouchClass.html#a5653e10917e82d6c60b0ad7a312c08d7":[13,0,4,0,12],
"classTouchDriver_1_1TouchClass.html#a6f520d5adc27baa6db381707a30ef2b8":[13,0,4,0,7],
"classTouchDriver_1_1TouchClass.html#a8ac3831f17f3b92b2e78374b4398f5d0":[13,0,4,0,14],
"classTouchDriver_1_1TouchClass.html#a947824db5fb8c72c857f2a28b96e3d11":[13,0,4,0,4],
"classTouchDriver_1_1TouchClass.html#a9ce8da088002da56558577501ae7bb9c":[13,0,4,0,10],
"classTouchDriver_1_1TouchClass.html#a9f12246ac9eee15050df7b7bb6e910b2":[13,0,4,0,8],
"classTouchDriver_1_1TouchClass.html#ab50fe2c1c5ae351f9f4eff56e48d3d40":[13,0,4,0,5],
"classTouchDriver_1_1TouchClass.html#ab697e1d6c4a61754f39b6f1a019c4996":[13,0,4,0,6],
"classTouchDriver_1_1TouchClass.html#ad4df9b906f5de16b26906667b82ffd78":[13,0,4,0,15],
"classTouchDriver_1_1TouchClass.html#ad64d7c9590aa929ba4421ff94cfe3198":[13,0,4,0,9],
"classTouchDriver_1_1TouchClass.html#adad320310fa3e1341c517bb61dc75166":[13,0,4,0,1],
"classTouchDriver_1_1TouchClass.html#af38dd4462dc4d0e198ad04dd384c1e04":[13,0,4,0,11],
"classencoder__driver_1_1encoder.html":[13,0,0,0],
"classencoder__driver_1_1encoder.html#a43c8c8aa9b831e71484508ba3875a311":[13,0,0,0,0],
"classes.html":[13,1],
"classmcp9808_1_1MCP9808.html":[13,0,1,0],
"classmcp9808_1_1MCP9808.html#a2ac6ec9984542aad945f176bc2915b63":[13,0,1,0,0],
"classmotor__driver_1_1MotorDriver.html":[13,0,2,0],
"classmotor__driver_1_1MotorDriver.html#af6a71e3b190d3378966f6df22feeeefa":[13,0,2,0,0],
"classscreendriver_1_1touchscreen.html":[13,0,3,0],
"classscreendriver_1_1touchscreen.html#a5e466e0c0b220f057b31386143a4e3c6":[13,0,3,0,0],
"encoder__driver_8py.html":[14,0,0],
"encoder__driver_8py.html#a197bc3ff15c0220f7f9b76eef0bb8851":[14,0,0,17],
"encoder__driver_8py.html#a2931b96bae700c805c77dacb689fe08d":[14,0,0,1],
"encoder__driver_8py.html#a3aed1b4b46107b226fd33ccdfbd8ce7d":[14,0,0,10],
"encoder__driver_8py.html#a3bd0c08e5b9625c28718d81fa9ebcc53":[14,0,0,7],
"encoder__driver_8py.html#a4d8732322583c8a41e37ffbe9b73eb95":[14,0,0,16],
"encoder__driver_8py.html#a602d8eaa7ba1e59fd7f1fbcb1635a567":[14,0,0,11],
"encoder__driver_8py.html#a6635b8decd187d98e82e40043a6840f9":[14,0,0,5],
"encoder__driver_8py.html#a83f9050e16f6b422a893b782e0fc115c":[14,0,0,6],
"encoder__driver_8py.html#a8f1c96b66c980ac71112c3786233b88d":[14,0,0,18],
"encoder__driver_8py.html#a8f53e20c54459233845d81664374815e":[14,0,0,14],
"encoder__driver_8py.html#aaa8d256fb0bb367749c4d3687249c6ed":[14,0,0,9],
"encoder__driver_8py.html#ab0eefff23f304bf55272736c08bbfe66":[14,0,0,2],
"encoder__driver_8py.html#ac3f6a1bad8b3528f013bfc6978714e2e":[14,0,0,15],
"encoder__driver_8py.html#ac5d1c8c87558ebcd94d1e8d7724f53e3":[14,0,0,4],
"encoder__driver_8py.html#ac6a00ad3321e6105392901cb258c3af7":[14,0,0,8],
"encoder__driver_8py.html#acef70077eabc0c2295586323c8b4a0ef":[14,0,0,3],
"encoder__driver_8py.html#ad98f3fc6bae9b4124a87746279a50f69":[14,0,0,13],
"encoder__driver_8py.html#adba8fcd935c284d6005524fab39a7495":[14,0,0,12],
"files.html":[14,0],
"functions.html":[13,2,0],
"functions_func.html":[13,2,1],
"functions_vars.html":[13,2,2],
"index.html":[],
"index.html#sec_intro":[0],
"index.html#sec_lab1":[1],
"index.html#sec_lab2":[2],
"index.html#sec_lab3":[3],
"index.html#sec_lab4":[4],
"index.html#sec_lab5":[5],
"index.html#sec_lab6":[6],
"index.html#sec_lab7":[7],
"index.html#sec_lab8":[8],
"index.html#sec_lab9":[9],
"lab3main_8py.html":[14,0,1],
"lab3main_8py.html#a0fc76b65445d57ebf6b0ebbb531543b0":[14,0,1,11],
"lab3main_8py.html#a343a1e377880ec955a40077354a92399":[14,0,1,6],
"lab3main_8py.html#a355608c9b44332866d3038371772ae9c":[14,0,1,8],
"lab3main_8py.html#a71737acc2e329eb7f1beadac85e3689c":[14,0,1,3],
"lab3main_8py.html#a88652427ff8d9c2572a7d50c41885cbd":[14,0,1,0],
"lab3main_8py.html#a8b59356ee3e0e964f8227401b6fc1c38":[14,0,1,1],
"lab3main_8py.html#a91ce2bbcfb4f2f89b5e2a3dcacb1c434":[14,0,1,12],
"lab3main_8py.html#aa37b47f7ee964b1434c87a20bd031ef9":[14,0,1,4],
"lab3main_8py.html#aa589c775681317d09a190a35e6cf9991":[14,0,1,2],
"lab3main_8py.html#aaffa4c3aa1a831fae1decf91bd84f0fc":[14,0,1,5],
"lab3main_8py.html#ab12834f8657766c4981a0676007575bd":[14,0,1,7],
"lab3main_8py.html#abea21f734ae15e00ac204d9c98de49ff":[14,0,1,10],
"lab3main_8py.html#adab61cf77cfbf6bb2e04a7e67eebfb2e":[14,0,1,9],
"lab4main_8py.html":[14,0,2],
"lab4main_8py.html#a1a4bd677a30028603115aacf511f123a":[14,0,2,7],
"lab4main_8py.html#a442d96d63a3f3e606e1553312bed4f87":[14,0,2,5],
"lab4main_8py.html#a55fe4661c02cf3315288cb661255cce3":[14,0,2,4],
"lab4main_8py.html#a6cae066aa4118751bd80ca451db883ce":[14,0,2,9],
"lab4main_8py.html#a762136809b99d784ac9fbc9e0e0a39d3":[14,0,2,2],
"lab4main_8py.html#a835a21606477234f092051643abf7cf9":[14,0,2,1],
"lab4main_8py.html#a87dc6ae28b17b07ba3a6140955ccc1ed":[14,0,2,0],
"lab4main_8py.html#ab5e621e61827991ba953abc53cd88f3b":[14,0,2,3],
"lab4main_8py.html#aca412a8b590e8f6de60b73632b5d529c":[14,0,2,10],
"lab4main_8py.html#acbfaba2284d58214699076ac69d3c0b0":[14,0,2,8],
"lab4main_8py.html#acff3297255fcc481065f01b155c357e9":[14,0,2,11],
"lab4main_8py.html#af645c155fd48c2cf08dc3ce28f1589bf":[14,0,2,6],
"mcp9808_8py.html":[14,0,4],
"mcp9808_8py.html#a089290bc0c9e4923b9c6d0b9368eddd3":[14,0,4,5],
"mcp9808_8py.html#a0bf36da4b043909964054e0f0dfdbac6":[14,0,4,3],
"mcp9808_8py.html#a3b3077760999ee4a74177a8c0d2d0b53":[14,0,4,7],
"mcp9808_8py.html#a3f79df53310fba52d4e83a2e6532e2e4":[14,0,4,17],
"mcp9808_8py.html#a47d1dbc7aea12ff1c695d77e0a7000f7":[14,0,4,15],
"mcp9808_8py.html#a51dc54774eb15fe8af9fa17140113b03":[14,0,4,13],
"mcp9808_8py.html#a54383af2693144482bf8fc0aa86883b1":[14,0,4,11],
"mcp9808_8py.html#a6c7535221effce70023fc8259f6e8827":[14,0,4,8],
"mcp9808_8py.html#a886781ce4d0ce303743334d89db87b27":[14,0,4,6],
"mcp9808_8py.html#a909d8914dd8071637706625e759873c7":[14,0,4,12],
"mcp9808_8py.html#a92cf1d27e08ad5b2aa56019baeb531cb":[14,0,4,16],
"mcp9808_8py.html#aa1193d87678d266302318e445bdd28cf":[14,0,4,14],
"mcp9808_8py.html#abb3b6e8f758d441f29781067664fff47":[14,0,4,1],
"mcp9808_8py.html#abcd5fb7681b5e2b409928ee1f61992bc":[14,0,4,4],
"mcp9808_8py.html#acbc0ae9b2c8af826881b5d9b05434bd2":[14,0,4,2],
"mcp9808_8py.html#ae99e487519ec535e9356de31b7f1dee7":[14,0,4,9],
"mcp9808_8py.html#aec4daa2220b4a9f26a6cbb2f38f9f1a2":[14,0,4,10],
"motor__driver_8py.html":[14,0,5],
"motor__driver_8py.html#a0027e4a6d1da9c2bf9cc71434f02d119":[14,0,5,16],
"motor__driver_8py.html#a27cb7992ada28554135afe383e4f4949":[14,0,5,10],
"motor__driver_8py.html#a2b03335301567235816074712e3159e1":[14,0,5,6],
"motor__driver_8py.html#a3167caf2a33e2e85b4d217924cdd9e99":[14,0,5,11],
"motor__driver_8py.html#a344e327c61595ff1a62f3713d33dd0f2":[14,0,5,14],
"motor__driver_8py.html#a40d1c689bfc571ebdbc9257a222da1b8":[14,0,5,3],
"motor__driver_8py.html#a4c49f59072bd3c251fbb22764d2ee913":[14,0,5,18],
"motor__driver_8py.html#a542acf24959e1d765462d34a10e574a9":[14,0,5,5],
"motor__driver_8py.html#a60eea38047479f24f71e63ac50f2a308":[14,0,5,9],
"motor__driver_8py.html#a60f939d9ce477510ee0ec5ba67c44a56":[14,0,5,21],
"motor__driver_8py.html#a62539821e285c9a3daf74e0d1e7ec804":[14,0,5,8],
"motor__driver_8py.html#a751d4305a28c02d3b84ab6b171959721":[14,0,5,2],
"motor__driver_8py.html#a824913078c49ae7d49c4a5b943337c1b":[14,0,5,15],
"motor__driver_8py.html#a8820fcdf37d5a498a10d8b8de336709e":[14,0,5,7],
"motor__driver_8py.html#ab044a065882a4956aa9e1cd07e150cc6":[14,0,5,13],
"motor__driver_8py.html#ab81d00bbe888939c2d8aa9b3be31fa6e":[14,0,5,4],
"motor__driver_8py.html#ac207ed398413f91add92bc737ffa0c86":[14,0,5,17],
"motor__driver_8py.html#ad30324a65e2a20d965479d0ac8730048":[14,0,5,12],
"motor__driver_8py.html#ae56553a7317827ac04fc22872a8397ec":[14,0,5,20],
"motor__driver_8py.html#af19d9a5aa046aa324b7d7ad3d650e18a":[14,0,5,19],
"motor__driver_8py.html#af54367376493502f98481191f6708ba0":[14,0,5,1],
"pages.html":[],
"screendriver_8py.html":[14,0,7],
"screendriver_8py.html#a06bf4e8fde921b14fc28ae9be31abe97":[14,0,7,7],
"screendriver_8py.html#a0c2d56097b953d33f0ac4ca68f14ef8f":[14,0,7,25],
"screendriver_8py.html#a0ea9e054ffeee2d6f876c08819553535":[14,0,7,21],
"screendriver_8py.html#a11d318c18772c10f1402dfbf5e9de227":[14,0,7,11],
"screendriver_8py.html#a2d69cd8a042453054bfbc019af361259":[14,0,7,22],
"screendriver_8py.html#a3329fc1a29682449058a1b92483abb35":[14,0,7,9],
"screendriver_8py.html#a4ee8fd15c4b7717ea41b968fd8aec2d1":[14,0,7,13],
"screendriver_8py.html#a6823d777b3ed1dfe7707323f355edc92":[14,0,7,8],
"screendriver_8py.html#a68669c7980fad9c6db1be4a40b5c72e1":[14,0,7,1],
"screendriver_8py.html#a8499f436590d5be040bb379f09f5eaf7":[14,0,7,15],
"screendriver_8py.html#a8824e4f8886ccc3027f62245a83e990e":[14,0,7,3],
"screendriver_8py.html#a8a9bf573f656061ce3609bbf10dc3550":[14,0,7,16],
"screendriver_8py.html#a99a9329b49884b49ea42573edf87ec7b":[14,0,7,12],
"screendriver_8py.html#a9a75d4b10d36d6e5c9119066cc934c3c":[14,0,7,6],
"screendriver_8py.html#a9d2abd4b939c606a2879d9c62e5a7b37":[14,0,7,20],
"screendriver_8py.html#aa0def2defe35be9b35f670f71edcabee":[14,0,7,18],
"screendriver_8py.html#aaa24fc16040957edb7600f3464227132":[14,0,7,2],
"screendriver_8py.html#ac22d96cf04bd61c8889987f500ab2a1c":[14,0,7,19],
"screendriver_8py.html#ac64b7f0a43bd880af7712027b9cf325a":[14,0,7,27],
"screendriver_8py.html#aca85091290834cc7d89c312e093f46e1":[14,0,7,23],
"screendriver_8py.html#acbea009f8a7aeb2cf79c1e994e1f3c6c":[14,0,7,26],
"screendriver_8py.html#acd3d10f2e40d0e1866126e805e0256e0":[14,0,7,4],
"screendriver_8py.html#adac84affdddea9ae29eced141a446246":[14,0,7,10],
"screendriver_8py.html#aec1376726c257dc325b925fee32fc6e5":[14,0,7,5],
"screendriver_8py.html#aef1121e955c7388b3bc3ff790c629a60":[14,0,7,14],
"screendriver_8py.html#afbb0b90f30c8b62e6bd912cc66b4704a":[14,0,7,17],
"screendriver_8py.html#afec614d1dd59c4c6e6b68fcd67575075":[14,0,7,24],
"":[13,0,3],
"":[13,0,2],
"":[13,0,0],
"":[13,0,1],
"":[13,0,4]
};
