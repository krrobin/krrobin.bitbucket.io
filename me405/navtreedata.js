/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Keanau Robin's Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Vending Machine (Lab 1)", "index.html#sec_lab1", null ],
    [ "Reaction Time (Lab 2)", "index.html#sec_lab2", null ],
    [ "Voltage Step Response (Lab 3)", "index.html#sec_lab3", null ],
    [ "Hot or Not? (Lab 4)", "index.html#sec_lab4", null ],
    [ "Feeling Tipsy? (Lab 5)", "index.html#sec_lab5", null ],
    [ "Simulation or Reality? (Lab 6)", "index.html#sec_lab6", null ],
    [ "Feeling Touchy (Lab 7)", "index.html#sec_lab7", null ],
    [ "Term Project Part 1 (Lab 8)", "index.html#sec_lab8", null ],
    [ "Term Project Part 2 (Lab 9)", "index.html#sec_lab9", null ],
    [ "Lab 5 Hand Calcs", "Lab5HandCalculations.html", [
      [ "Lab 5 Hand Calculations", "Lab5HandCalculations.html#sec_lab5details", null ]
    ] ],
    [ "Lab 6 Plots", "Lab6Plots.html", [
      [ "Lab 6 Plots", "Lab6Plots.html#sec_lab6details", null ]
    ] ],
    [ "Lab 9 Analytics", "Lab9Analytics.html", [
      [ "Lab 9 Analytics", "Lab9Analytics.html#sec_lab9analytics", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Lab5HandCalculations.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';