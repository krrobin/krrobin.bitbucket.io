 ## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is Keanau Robin's ME 305 and 405 Portfolio. It contains code for all lecture and lab assignments for ME 305 and 405.
#
#
#  ME 305 Documentation can be found here:
#
#  https://krrobin.bitbucket.io/me305
#
#  ME 405 Documentation can be found here:
#
#  https://krrobin.bitbucket.io/me405
#
#  @author Keanau Robin
#
#
#  @date January 5, 2021
#